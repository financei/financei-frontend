import { createQueryKeyStore } from '@lukemorales/query-key-factory';
import BinanceService from 'src/services/binanceService';

export const queryKeys = createQueryKeyStore({
  account: {
    list: () => ({
      queryKey: ['account'],
      queryFn: BinanceService.fetchAccount,
      retry: 3,
    }),
  },
  prices: {
    list: (coins: string[]) => ({
      queryKey: [coins],
      queryFn: () => BinanceService.fetchPrices(coins),
      retry: 3,
      retryDelay: 3000,
    }),
  },
  deposits: {
    list: () => ({
      queryKey: ['deposits'],
      queryFn: BinanceService.fetchDepositHistory,
      retry: 3,
      retryDelay: 5000,
    }),
  },
});
