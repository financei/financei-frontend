import { binanceApi } from 'src/boot/axios';

import {
  IBinanceAccount,
  IBinanceConvertHistory,
  IBinanceDepositHistory,
  IPriceTicker,
} from 'src/store/binance/state';

interface DepositHistoryResponse {
  success: boolean;
  data: IBinanceDepositHistory[];
}

interface ConvertHistoryResponse {
  list: IBinanceConvertHistory[];
}

export default class BinanceService {
  static async fetchAccount(): Promise<IBinanceAccount> {
    try {
      const { data } = await binanceApi.get<IBinanceAccount>('/account');

      return data;
    } catch (error) {
      console.error(error);
      return {} as IBinanceAccount;
    }
  }

  static async fetchDepositHistory(): Promise<IBinanceDepositHistory[]> {
    try {
      const { data } = await binanceApi.get<DepositHistoryResponse>(
        `/deposits?beginTime=${
          new Date().getTime() - 1000 * 60 * 60 * 24 * 30
        }&endTime=${new Date().getTime()}`
      );
      console.log('fetchDepositHistory', data);

      if (!data.success) throw new Error('Failed to fetch deposit history');

      return data.data;
    } catch (error) {
      console.error(error);
      return [];
    }
  }

  static async fetchTotalInvestment(): Promise<IBinanceConvertHistory[]> {
    try {
      const {
        data: { list },
      } = await binanceApi.get<ConvertHistoryResponse>('/total-investment');

      return list;
    } catch (error) {
      console.error(error);
      return [];
    }
  }

  static async fetchPrices(coins: string[]): Promise<Record<string, number>> {
    try {
      const { data } = await binanceApi.get<IPriceTicker[]>('/prices', {
        params: {
          coins,
        },
      });

      return data.reduce((acc, curr) => {
        const symbol = curr.symbol;
        acc[symbol] = parseFloat(curr.price);
        return acc;
      }, {} as Record<string, number>);
    } catch (error) {
      console.error(error);
      return {};
    }
  }
}
