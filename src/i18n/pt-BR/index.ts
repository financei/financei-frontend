export default {
  canWithdraw: 'Pode sacar',
  canDeposit: 'Pode depositar',
  canTrade: 'Pode negociar',
  balances: 'Saldos',
  failedToFetchDepositHistory: 'Falha ao buscar histórico de depósitos',
  fees: 'Taxas',
  summary: 'Resumo',
};
