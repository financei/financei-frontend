export default {
  canWithdraw: 'Can withdraw',
  canDeposit: 'Can deposit',
  canTrade: 'Can trade',
  balances: 'Balances',
  failedToFetchDepositHistory: 'Failed to fetch deposit history',
  fees: 'Fees',
  summary: 'Summary',
};
