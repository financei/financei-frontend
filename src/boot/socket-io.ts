import { IBinanceMiniTicker } from './../store/binance/state';
import { io, Socket } from 'socket.io-client';

interface BinanceSocketEvent {
  connect: () => void;
  disconnect: () => void;
  priceUpdate: (data: string[]) => void;
  priceUpdated: (data: IBinanceMiniTicker) => void;
}

type BinanceSocket = Socket<BinanceSocketEvent>;

const binanceSocket: BinanceSocket = io(process.env.SERVER_URL);

import { boot } from 'quasar/wrappers';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $binanceSocket: BinanceSocket;
  }
}

export default boot(({ app }) => {
  app.config.globalProperties.$binanceSocket = binanceSocket;
});

export { binanceSocket };
