export interface IBinanceAccount {
  canTrade?: boolean;
  canWithdraw?: boolean;
  canDeposit?: boolean;
  balances?: IBinanceBalance[];
}

export interface IBinanceBalance {
  asset: string;
  free: string;
  locked: string;
}

export interface IBinanceMiniTicker {
  curDayClose: string;
  eventTime: number;
  eventType: string;
  high: string;
  low: string;
  open: string;
  symbol: string;
  volume: string;
  volumeQuote: string;
}

export interface IBinanceDepositHistory {
  orderNo: string;
  fiatCurrency: string;
  indicatedAmount: string;
  amount: string;
  totalFee: string;
  method: string;
  status: 'Successful' | 'Pending' | 'Failed';
  createTime: number;
  updateTime: number;
}

export interface IBinanceConvertHistory {
  quoteId: string;
  orderId: number;
  orderStatus: string;
  fromAsset: string;
  fromAmount: string;
  toAsset: string;
  toAmount: string;
  ratio: string;
  inverseRatio: string;
  createTime: number;
}

export interface IPriceTicker {
  symbol: string;
  price: string;
}

export interface IBinanceState {
  account: IBinanceAccount;
  depositHistory: IBinanceDepositHistory[];
  convertHistory: IBinanceConvertHistory[];
  prices: Record<string, number>;
}

function state(): IBinanceState {
  return {
    account: {},
    depositHistory: [],
    convertHistory: [],
    prices: {},
  };
}

export default state;
