import { MutationTree } from 'vuex';
import {
  IBinanceAccount,
  IBinanceConvertHistory,
  IBinanceDepositHistory,
  IBinanceState,
  IPriceTicker,
} from './state';

export const SET_DEPOSIT_HISTORY = 'SET_DEPOSIT_HISTORY';
export const SET_TOTAL_INVESTMENT = 'SET_TOTAL_INVESTMENT';
export const SET_PRICES = 'SET_PRICES';
export const SET_ACCOUNT = 'SET_ACCOUNT';

const mutation: MutationTree<IBinanceState> = {
  [SET_ACCOUNT](state, payload: IBinanceAccount) {
    state.account = payload;
  },
  [SET_DEPOSIT_HISTORY](state, payload: IBinanceDepositHistory[]) {
    state.depositHistory = payload;
  },
  [SET_TOTAL_INVESTMENT](state, payload: IBinanceConvertHistory[]) {
    state.convertHistory = payload;
  },
  [SET_PRICES](state, payload: IPriceTicker) {
    const symbol = payload.symbol.replace('USDT', '');
    state.prices[symbol] = parseFloat(payload.price);
  },
};

export default mutation;
