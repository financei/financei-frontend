import { Module } from 'vuex';
import { StateInterface } from '../index';
import state, { IBinanceState } from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const binanceModule: Module<IBinanceState, StateInterface> = {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};

export default binanceModule;
