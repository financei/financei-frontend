import {
  SET_ACCOUNT,
  SET_TOTAL_INVESTMENT,
  SET_DEPOSIT_HISTORY,
  SET_PRICES,
} from './mutations';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { IBinanceMiniTicker, IBinanceState } from './state';
import { binanceSocket } from 'src/boot/socket-io';
import BinanceService from 'src/services/binanceService';

const actions: ActionTree<IBinanceState, StateInterface> = {
  async fetchAccount({ commit }) {
    const account = await BinanceService.fetchAccount();
    commit(SET_ACCOUNT, account);
  },

  async fetchDepositHistory({ commit }) {
    const depositHistory = await BinanceService.fetchDepositHistory();
    commit(SET_DEPOSIT_HISTORY, depositHistory);
  },

  async fetchTotalInvestment({ commit }) {
    const totalInvestment = await BinanceService.fetchTotalInvestment();
    commit(SET_TOTAL_INVESTMENT, totalInvestment);
  },

  async fetchPrices({ getters, commit }) {
    const prices = await BinanceService.fetchPrices(getters.getWatchedCoins);
    prices.forEach((price) => {
      commit(SET_PRICES, price);
    });
  },

  connectSocket({ getters, commit }) {
    binanceSocket.connect().on('connect', () => {
      binanceSocket
        .emit('priceUpdate', getters.getWatchedCoins)
        .on('priceUpdated', (data: IBinanceMiniTicker) => {
          commit(SET_PRICES, { symbol: data.symbol, price: data.curDayClose });
        });
    });
  },
};

export default actions;
