import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { IBinanceState } from './state';

interface DepositTotals {
  totalAmount: number;
  totalFee: number;
}

export type BinanceGetters = GetterTree<IBinanceState, StateInterface> & {
  getWatchedCoins(state: IBinanceState): string[];
  getDepositInfo(state: IBinanceState): DepositTotals;
};

const getters: BinanceGetters = {
  getBalances(state): IBinanceState['account']['balances'] {
    return state.account?.balances || [];
  },
  getWatchedCoins(state): string[] {
    return (
      state.account?.balances
        ?.filter((balance) => Number(balance.free) > 0)
        ?.map((balance) => balance.asset + 'USDT') || []
    );
  },
  getDepositInfo(state): DepositTotals {
    return state.depositHistory.reduce(
      (total, deposit) => {
        if (deposit.status === 'Successful') {
          total.totalAmount += parseFloat(deposit.amount);
          total.totalFee += parseFloat(deposit.totalFee);
        }
        return total;
      },
      {
        totalAmount: 0,
        totalFee: 0,
      } as DepositTotals
    );
  },
};

export default getters;
